package com.psybergate.grad2020.revision.annotations.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

import com.psybergate.grad2020.revision.annotations.domain.annotations.DomainClass;
import com.psybergate.grad2020.revision.annotations.domain.annotations.DomainProperty;
import com.psybergate.grad2020.revision.annotations.domain.annotations.DomainTransient;

/**
 * @author phetole.makgobola
 * @author motheo.sekgetho
 * @since 17 Apr 2020
 * @class Customer
 */
@DomainClass(tableName = "customer")
public class Customer {

  @DomainProperty(columnName = "customer_number", isPrimaryKey = true, isUnique = true)
  private final String customerNum; // Made to be final because customerNum stays constant

  @DomainProperty(columnName = "name")
  private String name;

  @DomainProperty(columnName = "surname")
  private String surname;

  @DomainProperty(columnName = "date_of_birth")
  private final Integer dateOfBirth; // Made to be final because date of birth stays the same

  @DomainTransient
  private int age;

  public Customer(final String customerNum, final String name, final String surname,
      final Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    calculateAge();
  }

  private final void calculateAge() {
    Date date;
    DateFormat dateFormat = new SimpleDateFormat("yyMMdd");

    try {
      if (dateOfBirth.intValue() >= 1_000_000) {
        throw new DateTimeException("Invalid Date Of Birth");
      }
      date = dateFormat.parse(getDateOfBirth().toString());
    }
    catch (Exception ex) {
      ex.printStackTrace();
      throw new RuntimeException(ex);
    }

    @SuppressWarnings("deprecation")
    LocalDate dateOfBirthAsLocalDate = LocalDate.of(date.getYear(), date.getMonth() + 1,
        date.getDate());
    Period period = Period.between(dateOfBirthAsLocalDate, LocalDate.now());
    int age = period.getYears() - 1900;
    this.age = age;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  public int getAge() {
    return age;
  }

  public String getFullName() {
    return String.format("%1s %2s", getName(), getSurname());
  }

  @Override
  public String toString() {
    return String.format("%1s , %2s , %3d", getCustomerNum(), getFullName(), getAge());
  }

}
