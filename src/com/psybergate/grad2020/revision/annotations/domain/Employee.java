package com.psybergate.grad2020.revision.annotations.domain;

import com.psybergate.grad2020.revision.annotations.domain.annotations.DomainClass;
import com.psybergate.grad2020.revision.annotations.domain.annotations.DomainProperty;

/**
 * 
 * @since 13 Feb 2020
 */
@DomainClass(tableName = "employee")
public class Employee {

  @DomainProperty(columnName = "employee_Number", isPrimaryKey = true, isUnique = true)
  private String employeeID;

  @DomainProperty(columnName = "employee_full_name", isNullable = false)
  private String fullName;

  public Employee(String employeeID, String fullName) {
    this.fullName = fullName;
    this.employeeID = employeeID;
  }

  public String getEmployeeID() {
    return employeeID;
  }

  public String getFullName() {
    return fullName;
  }

  public String getName() {
    try {
      String[] fullName = getFullName().split(" ");
      return fullName[0];
    }
    catch (Exception o_O) {
      return getFullName(); // What is happening here?
    }
  }

  public String getSurname() {
    try {
      String[] fullName = getFullName().split(" ");
      return fullName[fullName.length - 1];
    }
    catch (Exception o_O) {
      return getFullName();
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Employee other = (Employee) obj;
    if (!getEmployeeID().equals(other.getEmployeeID())) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public String toString() {
    return employeeID + " " + fullName;
  }

}
