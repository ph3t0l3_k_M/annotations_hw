package com.psybergate.grad2020.revision.annotations.domain.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author phetole.makgobola
 * @since 17 Apr 2020
 * @class DomainClass
 */
@Target({ ElementType.TYPE, ElementType.TYPE_USE, ElementType.TYPE_PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainClass {

  String tableName() default "";

}
