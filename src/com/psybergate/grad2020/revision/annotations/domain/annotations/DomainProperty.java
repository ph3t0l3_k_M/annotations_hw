package com.psybergate.grad2020.revision.annotations.domain.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author phetole.makgobola
 * @since 17 Apr 2020
 * @class DomainProperty
 */
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainProperty {

  String columnName();

  boolean isPrimaryKey() default false;

  boolean isForeignKey() default false;

  boolean isNullable() default false;

  boolean isUnique() default false;
}
