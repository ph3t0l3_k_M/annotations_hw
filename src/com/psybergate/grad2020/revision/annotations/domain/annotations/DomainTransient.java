package com.psybergate.grad2020.revision.annotations.domain.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author phetole.makgobola
 * @since 17 Apr 2020
 * @class DomainTransient
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainTransient {

  String value() default "";
}
