package com.psybergate.grad2020.revision.annotations.domain.client;

import com.psybergate.grad2020.revision.annotations.domain.Customer;
import com.psybergate.grad2020.revision.annotations.domain.Employee;
import com.psybergate.grad2020.revision.annotations.domain.utility.CustomerService;
import com.psybergate.grad2020.revision.annotations.domain.utility.EmployeeService;

/**
 * @author phetole.makgobola
 * @since 20 Apr 2020
 * @class MainClass
 */
public class MainClass {

  public MainClass() {
  }

  public static void main(String[] args) {
    try {
      final CustomerService custService = new CustomerService();
      final EmployeeService empService = new EmployeeService();

      custService.generateCustomerTable();
      empService.generateEmployeeTable();

      Customer object = new Customer("A101", "Sean", "Carter", 970627);
      custService.addCustomer(new Customer("A102", "Something", "Roccafella", 960420));
      custService.addCustomer(new Customer("A103", "Thomas", "Eddison", 780325));
      custService.addCustomer(new Customer("A104", "Henry", "Ford", 030206));

      Employee object2 = new Employee("A101", "Sean Carter");
      empService.saveEmployee(new Employee("A102", "Something Roccafella"));
      empService.saveEmployee(new Employee("A103", "Thomas Eddison"));
      empService.saveEmployee(new Employee("A104", "Henry Ford"));

      custService.addCustomer(object);
      empService.saveEmployee(object2);

      custService.removeCustomer("A103");
      empService.deleteEmployee("A103");

      System.out.println(custService.getCustomer("A101"));
      System.out.println(empService.getEmployee("A101"));
    }
    catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

}
