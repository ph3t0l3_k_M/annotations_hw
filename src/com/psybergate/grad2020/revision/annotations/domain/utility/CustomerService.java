package com.psybergate.grad2020.revision.annotations.domain.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.psybergate.grad2020.revision.annotations.domain.Customer;

/**
 * @author motheo.sekgetho
 * @author phetole.makgobola
 * @since 15 Apr 2020
 * @class CustomerService
 */
public class CustomerService {

  private int numCustomers = 0;

  private final DatabaseManager dbManager;

  private final List<Customer> customerList = new ArrayList<>();

  private final Map<String, Customer> customerMap = new HashMap<>();

  public final List<Customer> getCustomerList() {
    return this.customerList;
  }

  public CustomerService()
      throws Exception {
    dbManager = new DatabaseManager(
        DatabaseManager.loadDatabaseProperties("res/postgresDBProperties.txt"));
  }

  public final void generateCustomerTable() {
    try {
      dbManager.generateDatabase(Customer.class);
    }
    catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  public int getNumCustomers() {
    return this.numCustomers;
  }

  public final void addCustomer(Customer customer) {
    getCustomerList().add(customer);
    getCustomerMap().put(customer.getCustomerNum(), customer);
    this.numCustomers++;
    try {
      dbManager.insert(customer);
    }
    catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  public Map<String, Customer> getCustomerMap() {
    return this.customerMap;
  }

  public final Customer getCustomer(String customerNum) {
// Bro bro. Functionality for this had to change because I found to return the object straight from the database
    // You can make changes if you wish to.
    // Never mind what was said above (cwl)
    final Customer newCustomer = (Customer) dbManager.get(customerNum, Customer.class);
    getCustomerMap().put(newCustomer.getCustomerNum(), newCustomer);
    if (!getCustomerMap().containsKey(customerNum)) {
      throw new IllegalArgumentException("Customer Number Does Not Exist");
    }
    return getCustomerMap().get(customerNum);
  }

  public final void removeCustomer(String customerNum) {
    removeCustomer(getCustomer(customerNum));
  }

  // Moved db logic to the function below
  public final void removeCustomer(Customer customer) {
    dbManager.deleteObject(customer.getCustomerNum(), Customer.class);
    getCustomerList().remove(customer);
    getCustomerMap().remove(customer.getCustomerNum(), customer);
    numCustomers--;
  }

  public final void printCustomerList() {
    StringBuilder myCustomers = new StringBuilder();
    myCustomers.append("customer : \n");

    if (getCustomerList().size() < 1) {
      myCustomers.append("No customers available");
      System.out.println(myCustomers);
      return;
    }

    for (Customer customer : getCustomerList()) {
      StringBuilder row = new StringBuilder(
          "Name \t: " + customer.getFullName() + "\t\t ID : " + customer.getCustomerNum() + "\n");
      myCustomers.append(row);
    }
    System.out.println(myCustomers);
  }

  public final void printCustomerInfo() {
    for (Customer customer : getCustomerList()) {
      System.out.println(customer);
      System.out.println();
    }
  }
}
