package com.psybergate.grad2020.revision.annotations.domain.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

import com.psybergate.grad2020.revision.annotations.domain.annotations.DomainClass;
import com.psybergate.grad2020.revision.annotations.domain.annotations.DomainProperty;

/**
 * @author motheo.sekgetho
 * @author phetole.makgobola
 * @since 17 Apr 2020
 * @class DatabaseManager
 */
@SuppressWarnings("resource")
public class DatabaseManager {

  private String databaseSpecifiedURL;

  private Connection connection;

  public DatabaseManager(Properties properties)
      throws SQLException {

    setDatabaseConnectionURL(properties);
    setConnection(getDatabaseConnectionURL());

  }

  private void setDatabaseConnectionURL(Properties properties) {
    this.databaseSpecifiedURL = String.format(
        "jdbc:postgresql://localhost/%1s?user=%2s&password=%3s",
        properties.getProperty("databaseName"), properties.getProperty("username"),
        properties.getProperty("password"));
  }

  public Connection getConnection() {
    return connection;
  }

  private void setConnection(String url)
      throws SQLException {
    this.connection = DriverManager.getConnection(url);
  }

  public String getDatabaseConnectionURL() {
    return this.databaseSpecifiedURL;
  }

  public void closeDBConnection()
      throws SQLException {
    getConnection().close();
  }

  private String generateTableCreationQuery(Class<?> clazz) {
    StringBuilder query = new StringBuilder();
    TypeMapper typeMapper = TypeMapper.mapper;
    query.append("drop table if exists " + clazz.getSimpleName().toLowerCase() + ";\n create table "
        + clazz.getSimpleName().toLowerCase() + " ( ");

    Field[] fields = clazz.getDeclaredFields();
    for (int i = 0; i < fields.length; i++) {
      for (Annotation annotation : fields[i].getAnnotations()) {
        if (annotation instanceof DomainProperty) {
          DomainProperty domainProperty = fields[i].getAnnotation(DomainProperty.class);
          if (i > 0) {
            query.append(" , ");
          }
          String fieldName = domainProperty.columnName();
          query.append(fieldName + " " + typeMapper.getSqlTypeFromJavaType(fields[i].getType()));

          if (domainProperty.isPrimaryKey()) {
            query.append(" Primary Key ");
          }
        }
      }
    }
    query.append(" );");
    return query.toString();
  }

  public void generateDatabase(Class<?> clazz)
      throws SQLException {

    Statement statement = getConnection().createStatement();
    statement.execute(generateTableCreationQuery(clazz));

  }

  public void insert(Object object)
      throws SQLException, IllegalAccessException {
    Statement statement = getConnection().createStatement();
    statement.execute(generateInsertQuery(object));
  }

  private String generateInsertQuery(Object object)
      throws IllegalAccessException {
    Class<?> objectClass = object.getClass();
    Field[] fields = objectClass.getDeclaredFields();
    String columnAttributes = getColumnAttributes(fields);
    String objectValues = getObjectValues(object, fields);

    StringBuilder query = new StringBuilder(String.format("insert into %1s %2s values %3s;",
        object.getClass().getSimpleName(), columnAttributes, objectValues));

    return query.toString();
  }

  private static String getObjectValues(Object object, Field[] fields)
      throws IllegalAccessException {
    StringBuilder objectValues = new StringBuilder();
    objectValues.append("(");
    for (int i = 0; i < fields.length; i++) {
      for (Annotation annotation : fields[i].getAnnotations()) {
        if (annotation instanceof DomainProperty) {
          if (i > 0) {
            objectValues.append(",");
          }
          fields[i].setAccessible(true);
          objectValues.append("'" + fields[i].get(object) + "'");
          fields[i].setAccessible(false);

        }
      }
    }
    objectValues.append(")");
    return objectValues.toString();
  }

  private static String getColumnAttributes(Field[] fields) {
    StringBuilder columnAttributes = new StringBuilder();
    columnAttributes.append("(");
    for (int i = 0; i < fields.length; i++) {
      for (Annotation annotation : fields[i].getAnnotations()) {
        if (annotation instanceof DomainProperty) {
          if (i > 0) {
            columnAttributes.append(",");
          }
          DomainProperty domainProperty = fields[i].getAnnotation(DomainProperty.class);
          columnAttributes.append(domainProperty.columnName());
        }
      }
    }
    columnAttributes.append(")");
    return columnAttributes.toString();
  }

  public static Properties loadDatabaseProperties(String fileName)
      throws FileNotFoundException, IOException {
    Properties myDBProperties = new Properties();
    try (Reader fileReader = new FileReader(new File(fileName))) {
      myDBProperties.load(fileReader);
      return myDBProperties;
    }
  }

  public final Object get(final Object primaryKey, final Class<?> clazz) {
    final String primaryKeyName = getPrimaryKeyColumnName(clazz);
    final String tableName = clazz.getAnnotation(DomainClass.class).tableName();
    final String SqlStatement = String.format("select * from %1s where %2s = ?;", tableName,
        primaryKeyName);
    final List<Object> parameterList = new ArrayList<>();
    final List<Class<?>> constructorParameterTypeList = new ArrayList<>();
    try (final PreparedStatement ps = getConnection().prepareStatement(SqlStatement)) {
      ps.setObject(1, primaryKey);
      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          for (final Field field : clazz.getDeclaredFields()) {
            final DomainProperty property = field.getAnnotation(DomainProperty.class);
            if (property != null) {
              parameterList.add(rs.getObject(property.columnName()));
              constructorParameterTypeList.add(field.getType());
            }
          }
          final Class<?>[] typeParameters = constructorParameterTypeList.toArray(new Class<?>[0]);
          final Constructor<?> objConstructor = clazz.getDeclaredConstructor(typeParameters);
          return objConstructor.newInstance(parameterList.toArray(new Object[0]));
        }
        throw new NoSuchElementException("Record not found");
      }
      catch (final Exception ex) {
        throw new RuntimeException(ex);
      }

    }
    catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  private final String getPrimaryKeyColumnName(final Class<?> clazz) {
    for (final Field field : clazz.getDeclaredFields()) {
      final DomainProperty property = field.getAnnotation(DomainProperty.class);
      if (property != null) {
        if (property.isPrimaryKey())
          return property.columnName();
      }
    }
    throw new NoSuchElementException(
        "No Primary Key columns on table " + clazz.getSimpleName().toLowerCase());
  }

  public final void deleteObject(final Object primaryKey, final Class<?> claszz) {
    final String tableName = claszz.getAnnotation(DomainClass.class).tableName();
    final String primaryKeyName = getPrimaryKeyColumnName(claszz);
    final String SqlStatement = String.format("delete from %1s where %2s = ?;", tableName,
        primaryKeyName);

    try (final PreparedStatement ps = getConnection().prepareStatement(SqlStatement)) {
      ps.setObject(1, primaryKey);
      ps.execute();
      System.out.println("Deleted Successfully");
    }
    catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

}
