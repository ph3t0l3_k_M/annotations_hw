package com.psybergate.grad2020.revision.annotations.domain.utility;

import com.psybergate.grad2020.revision.annotations.domain.Employee;

/**
 * @author phetole.makgobola
 * @since 20 Apr 2020
 * @class EmployeeUtility
 */
public class EmployeeService {

  private final DatabaseManager dbManager;

  public EmployeeService()
      throws Exception {
    dbManager = new DatabaseManager(
        DatabaseManager.loadDatabaseProperties("res/postgresDBProperties.txt"));
  }

  public final void generateEmployeeTable() {
    try {
      dbManager.generateDatabase(Employee.class);
    }
    catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  public final void saveEmployee(final Employee employee) {
    try {
      dbManager.insert(employee);
    }
    catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  public final Employee getEmployee(final String employeeID) {
    return (Employee) dbManager.get(employeeID, Employee.class);
  }

  public final void deleteEmployee(final String employeeID) {
    dbManager.deleteObject(employeeID, Employee.class);
  }

}
