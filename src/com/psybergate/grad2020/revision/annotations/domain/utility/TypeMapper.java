package com.psybergate.grad2020.revision.annotations.domain.utility;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author phetole.makgobola
 * @since 17 Apr 2020
 * @class TypeMapper
 */
public class TypeMapper {

  private static final Map<Class<?>, String> JavaToSqlTypeMap = new HashMap<>();

  private static final Map<String, Class<?>> SqlToJavaTypeMap = new HashMap<>();

  public static final TypeMapper mapper = new TypeMapper();

  private static final void loadMaps() {
    JavaToSqlTypeMap.put(String.class, "text");
    JavaToSqlTypeMap.put(Integer.class, "integer");
    JavaToSqlTypeMap.put(int.class, "integer");
    JavaToSqlTypeMap.put(Long.class, "bigint");
    JavaToSqlTypeMap.put(long.class, "bigint");
    JavaToSqlTypeMap.put(Double.class, "real");
    JavaToSqlTypeMap.put(double.class, "real");
    JavaToSqlTypeMap.put(Float.class, "float");
    JavaToSqlTypeMap.put(float.class, "float");
    JavaToSqlTypeMap.put(Boolean.class, "boolean");
    JavaToSqlTypeMap.put(boolean.class, "boolean");
    JavaToSqlTypeMap.put(Character.class, "char(1)");
    JavaToSqlTypeMap.put(char.class, "char(1)");
    JavaToSqlTypeMap.put(LocalDate.class, "timestamp");
    JavaToSqlTypeMap.put(Date.class, "date");
    JavaToSqlTypeMap.put(java.util.Date.class, "date");

    // SqlType Map to be filled later??
  }

  private TypeMapper() {
    synchronized (TypeMapper.class) {
      loadMaps();
    }
  }

  public final String getSqlTypeFromJavaType(final Class<?> type) {
    final String sqlType = JavaToSqlTypeMap.get(type);
    if (sqlType != null) return sqlType;
    throw new NoSuchElementException("Java Type not supported");
  }

  // Needs more thought than I anticipated
  public final Class<?> getJavaTypeFromSqlType(final String sqlType) {
    final Class<?> javaType = SqlToJavaTypeMap.get(sqlType);
    if (javaType != null) return javaType;
    throw new NoSuchElementException("Sql Type not supported");
  }

}
